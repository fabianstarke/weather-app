import React from "react";
import styled from "styled-components";

const WeatherButton = styled.div`
  width: 60px;
  height: 40px;
  position: absolute;
  top: 10px;
  padding: 10px;
  right: 10px;
  padding: 0;
  color: #fff;
  display: flex;
  align-items: center;
  border-radius: 5px;
  font-size: 14px;
  justify-content: center;
  background-color: #0f0f0fec;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
  -webkit-box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
`;

const Button = props => (
  <WeatherButton>
    <img src={props.icon} width="30" height="30" />
		{props.current}°c
  </WeatherButton>
);

export default Button;
